// Structure de chaque contact : 
//	{nom: "prénom nom", fct: "président", tel: "06..." }
var listeContact = { contact:
    [
        {nom: "Gilles Durvin", fct: "Président", tel: "06 13 48 20 31"},
        {nom: "Jérôme Manhes", fct: "Secrétaire", tel: "06 86 48 32 60"},
        {nom: "Vincent Velio", fct: "Prof de Wing Chun", tel: "06 11 22 14 81"}
    ]
    };